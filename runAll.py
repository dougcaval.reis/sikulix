import unittest
import os
import unittest
import HtmlTestRunner

def run_tests():
    # Obtém o diretório atual
    current_directory = os.path.dirname(os.path.realpath(__file__))
    
    # Percorre todos os arquivos no diretório de testes
    for filename in os.listdir(current_directory):
        if filename.endswith('.py') and filename.startswith('pyTest'):
            # Importa o módulo de teste dinamicamente
            module_name = filename[:-3]  # Remove a extensão .py
            __import__(module_name)
            
    # Cria um objeto TestSuite e adiciona todos os testes
    test_suite = unittest.TestLoader().discover(current_directory, pattern='pyTest*.py')
    
    # Configura o caminho para o relatório HTML
    report_path = os.path.join(current_directory, 'new_test_report.html')
    
    # Executa os testes e gera o relatório HTML
    with open(report_path, 'w') as report_file:
        runner = HtmlTestRunner.HTMLTestRunner(stream=report_file, verbosity=3)
        runner.run(test_suite)


if __name__ == '__main__':
    run_tests()