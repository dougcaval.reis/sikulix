# -*- coding: utf-8 -*-
import subprocess
import unittest
import HtmlTestRunner

# Command to run SikuliX
comando_sikulix = "java -jar sikulixide.jar -r firstTest3.py"

# Run the SikuliX command on the command line
processo = subprocess.run(comando_sikulix, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

# Checks the process exit code
if processo.returncode == 0:
    print("SikuliX test passed successfully!")
else:
    print("Error running the SikuliX test.")
    print("Default Output:", processo.stdout.decode())
    print("Default Error:", processo.stderr.decode())

class PyTestSikuliCalculator3(unittest.TestCase):
    def test_passing(self):
        self.assertTrue(True)

if __name__ == "__main__": 
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output="reportsCalculator3.html"))