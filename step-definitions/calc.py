from behave import given, when, then

@given('that I have two numbers, {num1:d} and {num2:d}')
def step_impl(context, num1, num2):
    context.num1 = num1
    context.num2 = num2

@when('I add up the numbers')
def step_impl(context):
    context.result = context.num1 + context.num2

@then('the result should be {expected_result:d}')
def step_impl(context, expected_result):
    assert context.result == expected_result, f"Resultado esperado: {expected_result}, Resultado atual: {context.result}"
