Feature: Calculator
    As a user
    I want to use the calculator
    To add two numbers

Scenario: Sum of two numbers
    Given that I have two numbers, 5 and 7
    When I add up the numbers
    Then the result should be 12
