# Importa o módulo Sikuli
import sys
sys.path.append("sikulixide.jar")
from sikuli import wait, click, type


def test_sum_calculator():
    # Open Windows Calculator
    wait("Images\Windows_Start_Menu.png", 10)
    click("Images\Windows_Start_Menu.png")
    type("calculadora")
    wait("Images\Calculator_App.png", 10)
    click("Images\Calculator_App.png")
    
    # Click on the "1" button
    wait("Images\Calculator_1_Button.png", 10)
    click("Images\Calculator_1_Button.png")
    
    # Click on the "+" (sum) button
    wait("Images\Calculator_Add_Button.png", 10)
    click("Images\Calculator_Add_Button.png")
    
    # Click on the "1" button
    wait("Images\Calculator_1_Button.png", 10)
    click("Images\Calculator_1_Button.png")
    
    # Click on the "=" (equal) button
    wait("Images\Calculator_Equal_Button.png", 10)
    click("Images\Calculator_Equal_Button.png")
    
    # Checks if the result is "2"
    wait("Images\Calculator_Result_2.png", 10)
    print("Resultado da soma: 1 + 1 = 2")

    # Close Calculator
    wait("Images\Calculator_Close_Button.png", 10)
    click("Images\Calculator_Close_Button.png")

# Run the test
test_sum_calculator()
