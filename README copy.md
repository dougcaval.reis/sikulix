## Before run, install sikulix-2.0.5

# To run sikuli test automation file, single file
java -jar sikulixide.jar -r firstTest.py  
java -jar sikulixide.jar -r firstTest2.py  
java -jar sikulixide.jar -r firstTest3.py  
*don't generate reports*  
*automation works*  

# To run python Test , single file
python pyTest.py  
python pyTest2.py  
python pyTest3.py  
*generate reports*  
*headless*  

-newfolder
  |-singleReport.html

# to run all tests
python runAll.py  
*generate reports*  
-reports  
  |-TestResults_pyTest*.TestSikuliCalculator1.html  
  |-TestResults_pyTest*.TestSikuliCalculator2.html  
  |-TestResults_pyTest*.TestSikuliCalculator3.html  